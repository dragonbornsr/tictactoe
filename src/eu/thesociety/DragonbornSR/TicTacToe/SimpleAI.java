package eu.thesociety.DragonbornSR.TicTacToe;

import eu.thesociety.DragonbornSR.TicTacToe.GameLogic.Combination;
import eu.thesociety.DragonbornSR.TicTacToe.TileSquare.Owner;

/**
 * AI that can be won.
 * @author ROG
 *
 */
public class SimpleAI extends ComputerAI {
	
	/**
	 * Simple AI will recognize Players 2 in a row and block it, and also
	 * his own. If there are no 2 in a row's, then AI will select randomly.
	 * @param parent
	 */
	public SimpleAI(GameLogic parent) {
		this.parent = parent;
	}
	
	@Override
	public boolean makeMove() {
		
		//Look if AI can win!
		TileSquare move = this.getTwoInRow(Owner.AI);
		if (move != null) {
			move.setOwnerAI();
			return true;
		}
		//Look if Player can win! If can, then block it!
		move = this.getTwoInRow(Owner.PLAYER);
		if (move != null) {
			move.setOwnerAI();
			return true;
		}
		
		
		if (parent.q5.owner == Owner.NULL) {
			parent.q5.setOwnerAI();
			return true;
		}
		
		move = this.getOppositeCorner(Owner.PLAYER);
		if (move != null) {
			move.setOwnerAI();
			return true;
		}
		move = this.getOppositeCorner(Owner.AI);
		if (move != null) {
			move.setOwnerAI();
			return true;
		}
		
		move = this.getNextCorner(Owner.PLAYER);
		if (move != null) {
			move.setOwnerAI();
			return true;
		}
		move = this.getNextCorner(Owner.AI);
		if (move != null) {
			move.setOwnerAI();
			return true;
		}
		
		
		for (int i = 0; i <= 2; i++) {
			Combination c = parent.comb.get(i);
			if (c.a.owner == Owner.NULL) {
				c.a.setOwnerAI(); 
				return true;
			}
			if (c.b.owner == Owner.NULL) {
				c.b.setOwnerAI(); 
				return true;
			}
			if (c.c.owner == Owner.NULL) {
				c.c.setOwnerAI(); 
				return true;
			}
		}
		parent.restartGame();
		return false;
	}

	@Override
	public String getName() {
		return "Simple AI";
	}
}
