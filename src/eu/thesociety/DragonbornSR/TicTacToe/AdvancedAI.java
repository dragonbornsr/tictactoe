package eu.thesociety.DragonbornSR.TicTacToe;

import eu.thesociety.DragonbornSR.TicTacToe.GameLogic.Combination;
import eu.thesociety.DragonbornSR.TicTacToe.TileSquare.Owner;

/**
 * Ai that is supposed not to lose at all.
 * @author ROG
 *
 */
public class AdvancedAI extends ComputerAI {
	
	/**
	 * Constructor. Sets AI parent logic (Game board)
	 * @param parent (Game Board)
	 */
	public AdvancedAI(GameLogic parent) {
		this.parent = parent;
	}
	
	@Override
	public boolean makeMove() {
		
		//Look if AI can win!
		TileSquare move = this.getTwoInRow(Owner.AI);
		if (move != null) {
			move.setOwnerAI();
			return true;
		}
		//Look if Player can win! If can, then block it!
		move = this.getTwoInRow(Owner.PLAYER);
		if (move != null) {
			move.setOwnerAI();
			return true;
		}
		
		if (parent.q5.owner == Owner.NULL) {
			parent.q5.setOwnerAI();
			return true;
		}
		
		move = this.getJumpoverSituation(Owner.PLAYER);
		if (move != null) {
			move.setOwnerAI();
			return true;
		}
		
		move = this.getOppositeCorner(Owner.PLAYER);
		if (move != null) {
			move.setOwnerAI();
			return true;
		}
		
		move = this.getNextCorner(Owner.AI);
		if (move != null) {
			move.setOwnerAI();
			return true;
		}
		
		move = this.getNextCorner(Owner.PLAYER);
		if (move != null) {
			move.setOwnerAI();
			return true;
		}
		
		move = this.getOppositeCorner(Owner.AI);
		if (move != null) {
			move.setOwnerAI();
			return true;
		}
		
		move = this.getNextToFace(Owner.PLAYER);
		if (move != null) {
			move.setOwnerAI();
			return true;
		}		
		
		move = this.getOppositeFace(Owner.PLAYER);
		if (move != null) {
			move.setOwnerAI();
			return true;
		}
		
		move = this.getOppositeFace(Owner.AI);
		if (move != null) {
			move.setOwnerAI();
			return true;
		}
		
		
		for (int i = 0; i <= 2; i++) {
			Combination c = parent.comb.get(i);
			if (c.a.owner == Owner.NULL) {
				c.a.setOwnerAI(); 
				return true;
			}
			if (c.b.owner == Owner.NULL) {
				c.b.setOwnerAI(); 
				return true;
			}
			if (c.c.owner == Owner.NULL) {
				c.c.setOwnerAI(); 
				return true;
			}
		}
		return false;
	}

	@Override
	public String getName() {
		return "Advanced AI";
	}
}
