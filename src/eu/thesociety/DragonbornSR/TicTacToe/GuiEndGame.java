package eu.thesociety.DragonbornSR.TicTacToe;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.event.interfaces.IKeyEvent;
import com.coffeagame.Engine.gui.Gui;
import com.coffeagame.Engine.media.ResourceManager;

import eu.thesociety.DragonbornSR.TicTacToe.TileSquare.Owner;

/**
 * Gui that enables when game has ended (Draw/Win/Lose).
 * @author ROG
 *
 */
public class GuiEndGame extends Gui implements IKeyEvent {
	
	/**
	 * gameLogic class.
	 */
	private GameLogic gamelogic;
	/**
	 * Current winner.
	 */
	private Owner winner;
	
	/**
	 * Win, lose, draw textures.
	 */
	private Texture win, loose, draw, current;
	
	/**
	 * Constructor.
	 * @param gamelogic gameLogic
	 */
	public GuiEndGame(GameLogic gamelogic) {
		this.register(Integer.MAX_VALUE - 1);
		this.gamelogic = gamelogic;
		this.visible = false;
		
		win = ResourceManager.getTexture("win");
		loose = ResourceManager.getTexture("loose");
		draw = ResourceManager.getTexture("draw");
		current = draw;
	}
	
	@Override
	public void update(float delta) {

	}
	
	@Override
	public void draw(float delta) {
		Game.startGLMode2D();
		GL11.glPushMatrix();
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, 
					GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glEnable(GL11.GL_BLEND);
			Color.white.bind();
			current.bind();
			GL11.glBegin(GL11.GL_QUADS);
				GL11.glTexCoord2f(0, 0);
				GL11.glVertex2f(0, 0);
				GL11.glTexCoord2f(1, 0);
				GL11.glVertex2f(Game.SCREEN_WIDTH, 0);
				GL11.glTexCoord2f(1, 1);
				GL11.glVertex2f(Game.SCREEN_WIDTH, 
						Game.SCREEN_HEIGHT);
				GL11.glTexCoord2f(0, 1);
				GL11.glVertex2f(0, Game.SCREEN_HEIGHT);
			GL11.glEnd();
		GL11.glPopMatrix();
		Game.endGLMode2D();
	}
	
	/**
	 * 
	 * @param owner
	 */
	public void set(Owner owner) {
		this.winner = owner;
		this.visible = true;
		
		if (winner == Owner.PLAYER) {
			current = win;
		} else if (winner == Owner.AI) {
			current = loose;
		}
		if (winner == Owner.NULL) {
			current = draw;
		}
		
		for (TileSquare ts : gamelogic.tsList) {
			ts.isGamePaused = true;
		}
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void keyPressed(int key) {
		if ((key == Keyboard.KEY_SPACE) && this.visible) {
			gamelogic.restartGame();
			for (TileSquare ts : gamelogic.tsList) {
				ts.isGamePaused = false;
			}
			this.visible = false;
		}
	}

	@Override
	public void keyReleased(int key) {
		// TODO Auto-generated method stub
		
	}
}
