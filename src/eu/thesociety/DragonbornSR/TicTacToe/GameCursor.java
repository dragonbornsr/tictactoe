package eu.thesociety.DragonbornSR.TicTacToe;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import com.coffeagame.Engine.Objects.Cursor;
import com.coffeagame.Engine.event.interfaces.IKeyEvent;
import com.coffeagame.Engine.media.ResourceManager;

/**
 * Child of Cursor. defines cursor modes.
 * @author ROG
 *
 */
public class GameCursor extends Cursor implements IKeyEvent {
	
	/**
	 * Constructor.
	 */
	public GameCursor() {
		cursor = ResourceManager.getTexture("Cursor");
		Mouse.setGrabbed(true);
		cursorMode = 0;
	}
	
	@Override
	public void keyPressed(int key) {
		if (key == Keyboard.KEY_F3) {
			cursorMode += 1;
			switch (cursorMode) {
				case 0:
					this.visible = true;
					Mouse.setGrabbed(true);
					break;
				case 1:
					this.visible = false;
					Mouse.setGrabbed(false);
					break;
				default:
					cursorMode = 0;
					this.visible = true;
					Mouse.setGrabbed(true);
					break;
			}
		}
	}
}
