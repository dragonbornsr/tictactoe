package eu.thesociety.DragonbornSR.TicTacToe;

import com.coffeagame.Engine.Objects.GameObject;
import com.coffeagame.Engine.media.ResourceManager;
/**
 * Class that controlls game states.
 * @author ROG
 *
 */
public class GameStateManager extends GameObject {
	
	/**
	 * Enum that holds different states.
	 * @author ROG
	 *
	 */
	public static enum GameState {
		/** Initializing state. */
		INITIALIZING (0),
		/** Loading State. */
		LOADING (1),
		/** Playing State. */
		PLAYING(2);
		/** Game State. */
		private final int state;
		
		/**
		 * Constructor.
		 * @param state2 GameState.
		 */
		GameState(int state) {
			this.state = state;
		}
		/**
		 * Method to get game State.
		 * @return game state.
		 */
		public int getState() {
			return state;
		}
	}
	/**
	 * Current game State.
	 */
	public GameState gameState = GameState.INITIALIZING;
	
	/**
	 * Construcor. Registers gameStateManager to GameObjects list.
	 */
	public GameStateManager() {
		this.register(0);
	}
	
	@Override
	public void update(float delta) {
		if ((!ResourceManager.load) 
				&& (this.gameState == GameState.INITIALIZING)) {
			this.gameState = GameState.PLAYING;
			new GuiBackground();
			new GameLogic();
			new GameCursor();
		}
	}
}
