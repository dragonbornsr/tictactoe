package eu.thesociety.DragonbornSR.TicTacToe;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.glu.GLU;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.scene.GLViewPort;

/**
 * ViewPort class.
 * @author ROG
 *
 */
public class ViewPort extends GLViewPort {
	
	/**
	 * constructor.
	 */
	 public ViewPort() {
		this.zNear = 0.01f;
		this.zFar = 100f;
     	try {
				setupGLDisplayforTicTacToe();
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
      }
	
	 /**
	  * 
	  * @throws FileNotFoundException File not found
	  * @throws IOException IOException
	  */
	private static void setupGLDisplayforTicTacToe() throws 
				FileNotFoundException, IOException {
		/*
		 * Set Display Modes avabile.
		 * 0 - Small Screenwith border
		 * 1 - Full Screen with border
		 * 2 - Full Screen without border
		 */
		
		try {
			Display.setDisplayMode(new DisplayMode(512,512));
			Game.SCREEN_WIDTH = 512;
			Game.SCREEN_HEIGHT = 512;
			Display.setTitle("TicTacToe by Oliver Tiit");
			
			try {
			    ByteBuffer[] icons = new ByteBuffer[1];
			    icons[0] = getGameIcon("resources/icon_16.png",
			    		16, 16);
			    Display.setIcon(icons);
			} catch (IOException ex) {
			    ex.printStackTrace();
			}
			
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		/*
		 * Set up 2D and 3D viewports!
		 */
		Display.setVSyncEnabled(Game.vSync);
		//GL11.glEnable(GL11.GL_CULL_FACE);
		//GL11.glCullFace(GL11.GL_BACK);
		//GL11.glEnable(GL11.GL_FOG);
		GL11.glGetFloat(GL11.GL_PROJECTION_MATRIX, 
				Game.perspectiveProjectionMatrix);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		//GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
		GLU.gluPerspective(45.0f, 
				(float)Display.getWidth()/Display.getHeight(), 
				0.01f, 100f);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, Display.getWidth(), 
				Display.getHeight(), 0, 1, -1);
		
		GL11.glGetFloat(GL11.GL_PROJECTION_MATRIX, 
				Game.orthographicProjectrionMatrix);
		GL11.glLoadMatrix(Game.perspectiveProjectionMatrix);
		GL11.glMatrixMode(GL11.GL_MODELVIEW_MATRIX);
		
		Game.fbo = GL30.glGenFramebuffers();
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, Game.fbo);

		Game.rbo = GL30.glGenRenderbuffers();
		GL30.glBindRenderbuffer(GL30.GL_RENDERBUFFER, Game.rbo);
		GL30.glRenderbufferStorage(GL30.GL_RENDERBUFFER, 
				GL11.GL_RGBA8, 640, 480);
		GL30.glFramebufferRenderbuffer(GL30.GL_FRAMEBUFFER, 
				GL30.GL_COLOR_ATTACHMENT0, GL30.GL_RENDERBUFFER, 
				Game.rbo);

		assert(GL30.glCheckFramebufferStatus(GL30.GL_FRAMEBUFFER) == 
				GL30.GL_FRAMEBUFFER_COMPLETE);

		GL30.glBindFramebuffer(GL30.GL_DRAW_FRAMEBUFFER, Game.fbo);
		GL20.glDrawBuffers(GL30.GL_COLOR_ATTACHMENT0);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		
	}

}
