package eu.thesociety.DragonbornSR.TicTacToe;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.media.ResourceManager;
import com.coffeagame.Engine.scene.GLViewPort;

/**
 * Game class.
 * @author ROG
 *
 */
public class TicTacToe extends Game {

	/** resource manager. */
	ResourceManager resourceManager;
	/** Game cursor. */
	static GameCursor cursor;
	
	/** Player viewport (2D). */
	ViewPort vp;

	
	@Override
	protected void preInit() {
		super.init();
		
		th.setMaxTPS(60);
		currentDisplayMode = 0;
	}
	
	@Override
	protected void init() {
		super.init();
		/*Start Here*/
		new GameStateManager();
		vp =  new ViewPort();
		vp.setAspectRatio(1.0f);
		listOfCoreObjects.add(keyController);
		listOfCoreObjects.add(mouseController);
		listOfCoreObjects.add(gameEventController);
		resourceManager = new ResourceManager();
	}
	
	@Override
	protected void postInit() {
		super.postInit();
		/*Start Here*/
	}
	

	@Override 
	public void drawLoop() {
		/*
		 * Clear OpenGL window before drawing all objects.
		 */
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT 
				| GL11.GL_DEPTH_BUFFER_BIT);
		super.drawLoop();
		
		GL30.glBindFramebuffer(GL30.GL_READ_FRAMEBUFFER, fbo);
		GL11.glReadBuffer(GL30.GL_COLOR_ATTACHMENT0);
		GL30.glBindFramebuffer(GL30.GL_DRAW_FRAMEBUFFER, 0);
		GL20.glDrawBuffers(GL11.GL_BACK_LEFT);

		GL30.glBlitFramebuffer(0, 0, Display.getWidth(), 
				Display.getHeight(), 0, 0, Display.getWidth(), 
				Display.getHeight(), GL11.GL_COLOR_BUFFER_BIT, 
				GL11.GL_NEAREST);
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
		
	}
	/**
	 * Start drawing in 2D.
	 */
	public static void startGLMode2D() { GLViewPort.startGLMode2D(); }
	/**
	 * End drawing in 2D.
	 */
	public static void endGLMode2D() { GLViewPort.endGLMode2D(); }
	
}