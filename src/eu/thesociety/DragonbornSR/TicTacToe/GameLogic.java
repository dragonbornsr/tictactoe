package eu.thesociety.DragonbornSR.TicTacToe;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.TextureImpl;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.event.interfaces.IKeyEvent;
import com.coffeagame.Engine.gui.Gui;
import com.coffeagame.Engine.media.ResourceManager;

import eu.thesociety.DragonbornSR.TicTacToe.TileSquare.Owner;

/**
 * Class that holds game logic.
 * @author ROG
 *
 */
public class GameLogic extends Gui implements IKeyEvent{
	
	/**
	 * Defines font color of text.
	 */
	private Color fontcolor = new Color(50,50,50);
	
	/**
	 * Initializes font.
	 */
	private TrueTypeFont font;
	/**
	 * Initializes awt type font.
	 */
	Font awtFont = null;
	
	/**
	 * Initializes list of winning combinations.
	 */
	public ArrayList<Combination> comb = new ArrayList<Combination>();
	/**
	 * List of possible AIs.
	 */
	private ArrayList<ComputerAI> AIList = new ArrayList<ComputerAI>();
	/**
	 * all tiles. (represents the game board)
	 */
	public TileSquare q1, q2, q3, q4, q5, q6, q7, q8, q9;
	/**
	 * List of TileSquares.
	 */
	public List<TileSquare> tsList = new ArrayList<TileSquare>();
	/**
	 * Currently selected AI.
	 */
	private ComputerAI selectedAI;
	/**
	 * GUI that appears when game has ended (win/lose/draw).
	 */
	public GuiEndGame guiEndGame;
	/**
	 * Boolean that tells us if AI will start.
	 */
	private boolean aiStarts = false;
	
	/**
	 * Enum that describes different AI types.
	 * @author ROG
	 *
	 */
	public enum AI {
		/**
		 * SimpleAI that can be beaten.
		 */
		SIMPLE (0),
		/**
		 * Advanced AI that can not be beaten.
		 */
		ADVANCED (1);
		/**
		 * current ai.
		 */
		private final int ai;
		/**
		 * set ai.
		 * @param ai AI.
		 */
		AI(int ai) {
			this.ai = ai;
		}
		/**
		 * Method to get AI enum.
		 * @return AI enum.
		 */
		public int getAI() {
			return ai;
		}
	}
	
	/**
	 * Game Logic constructor.
	 */
	public GameLogic() {
		this.register(100);
		q1 = new TileSquare(this, 1, 0, 0 );
		q2 = new TileSquare(this, 2, 171, 0 );
		q3 = new TileSquare(this, 3, 342, 0 );
		q4 = new TileSquare(this, 4, 0, 171 );
		q5 = new TileSquare(this, 5, 171, 171 );
		q6 = new TileSquare(this, 6, 342, 171 );
		q7 = new TileSquare(this, 7, 0, 342 );
		q8 = new TileSquare(this, 8, 171, 342 );
		q9 = new TileSquare(this, 9, 342, 342 );
		
		tsList.add(q1);
		tsList.add(q2);
		tsList.add(q3);
		tsList.add(q4);
		tsList.add(q5);
		tsList.add(q6);
		tsList.add(q7);
		tsList.add(q8);
		tsList.add(q9);
		
		comb.add(new Combination(q1,q2,q3));
		comb.add(new Combination(q4,q5,q6));
		comb.add(new Combination(q7,q8,q9));
		comb.add(new Combination(q1,q4,q7));
		comb.add(new Combination(q2,q5,q8));
		comb.add(new Combination(q3,q6,q9));
		comb.add(new Combination(q1,q5,q9));
		comb.add(new Combination(q3,q5,q7));
		
		AIList.add(new SimpleAI(this));
		AIList.add(new AdvancedAI(this));
		
		selectedAI = AIList.get(AI.ADVANCED.getAI());
		
		awtFont = ResourceManager.getFont("TimesNewRoman");
		awtFont = awtFont.deriveFont(Font.BOLD, 20.0f);
		font = new TrueTypeFont(awtFont, true);
		
		guiEndGame = new GuiEndGame(this);
	}

	@Override
	public void update(float delta) {
	}
	
	@Override
	public void draw(float delta) {
		
		Game.startGLMode2D();

		GL11.glPushMatrix();
			TextureImpl.bindNone();
			Color.white.bind();
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, 
					GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glEnable(GL11.GL_BLEND);
			font.drawString(this.posX + 5, this.posY + 5, 
				"Playing against: " + selectedAI.getName(), 
				fontcolor);
			GL11.glDisable(GL11.GL_BLEND);
		GL11.glPopMatrix();
		Game.endGLMode2D();
		
	}
	
	@Override
	public void tilePressed(int id) {
		Owner winner = checkForWinner();
		if (winner != Owner.NULL) {
			if (winner == Owner.PLAYER) {
				guiEndGame.set(Owner.PLAYER); 
			}
			else {
				guiEndGame.set(Owner.AI);
			}	
		}
		else
		{
			if (selectedAI.makeMove()) {
				winner = checkForWinner();
				if (winner != Owner.NULL) {
					if (winner == Owner.PLAYER) {
						guiEndGame.set(Owner.PLAYER);
					}
					else {
						guiEndGame.set(Owner.AI);
					}
				}
			} else {
				guiEndGame.set(Owner.NULL);
			}
		}
		
	}
	
	/**
	 * Checks if AI or Player has won (also checks for draw.
	 * @return winner.
	 */
	public Owner checkForWinner() {
		for (Combination a : comb) {
			Owner w = a.getWinner();
			if (w != Owner.NULL) {
				return w;
			}
		}
		for (TileSquare ts : tsList) {
			if (ts.owner == Owner.NULL) {
				return Owner.NULL;
			}
		}
		guiEndGame.set(Owner.NULL);
		return Owner.NULL;
	}
	
	/**
	 * Method to restart the game.
	 */
	public void restartGame() {
		aiStarts = !aiStarts;
		for (TileSquare ts : tsList) {
			ts.reset();
		}
		if (aiStarts) {
			selectedAI.makeMove();
		}
	}
	
	/**
	 * Class that describes different winning combinatons.
	 * @author ROG
	 *
	 */
	public class Combination {
		/**
		 * Combination parts.
		 */
		TileSquare a, b, c;
		/**
		 * Costructor.
		 * @param a part 1
		 * @param b part 2
		 * @param c part 3
		 */
		Combination(TileSquare a, TileSquare b, TileSquare c) {
			this.a = a;
			this.b = b;
			this.c = c;
		}
		
		/**
		 * Returns Owner.NULL when there is no winner,
		 * else, returns Owner.PLAYER or Owner.AI.
		 * @return winner
		 */
		public Owner getWinner() {
			if ((Owner.PLAYER == this.a.owner) 
					&& (Owner.PLAYER == this.b.owner) 
					&& (Owner.PLAYER == this.c.owner)) {
				return Owner.PLAYER;
			}
			if ((Owner.AI == this.a.owner) 
					&& (Owner.AI == this.b.owner) 
					&& (Owner.AI == this.c.owner)) {
				return Owner.AI;
			}
			return Owner.NULL;
		}
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void keyPressed(int key) {
		if (key == Keyboard.KEY_F1) {
			//Open Help Sceen
		}
		if (key == Keyboard.KEY_F2) {
			if (selectedAI instanceof AdvancedAI) { 
				selectedAI = AIList.get(AI.SIMPLE.getAI());
			}
			else {
				selectedAI = AIList.get(AI.ADVANCED.getAI());
			}
			restartGame();
		}
		
	}
	
	/**
	 * 
	 * @param owner (winner of the game)
	 */
	public void endGame(Owner owner) {
		
	}

	@Override
	public void keyReleased(int key) {
		// TODO Auto-generated method stub
		
	}
}
