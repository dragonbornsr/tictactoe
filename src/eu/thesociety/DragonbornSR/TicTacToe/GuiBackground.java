package eu.thesociety.DragonbornSR.TicTacToe;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;

import com.coffeagame.Engine.Game;
import com.coffeagame.Engine.event.interfaces.IKeyEvent;
import com.coffeagame.Engine.gui.Gui;
import com.coffeagame.Engine.media.ResourceManager;

/**
 * GUI that draws background image to game.
 * @author ROG
 *
 */
public class GuiBackground extends Gui implements IKeyEvent {
	
	/**
	 * Background texture.
	 */
	Texture bg;
	
	/**
	 * Constructor.
	 */
	public GuiBackground() {
		bg = ResourceManager.getTexture("bg");
		this.height = Display.getHeight();
		this.width = Display.getWidth();
		this.register(0);
	}
	
	@Override
	public void update(float delta) {
		
	}	
	
	@Override
	public void draw(float delta) {
		Game.startGLMode2D();
		GL11.glPushMatrix();
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, 
					GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glDisable(GL11.GL_BLEND);
			bg.bind();
			GL11.glBegin(GL11.GL_QUADS);
				GL11.glTexCoord2f(0,0);
				GL11.glVertex2f(posX, posY);
				GL11.glTexCoord2f(1,0);
				GL11.glVertex2f(posX + width, posY);
				GL11.glTexCoord2f(1,1);
				GL11.glVertex2f(posX + width, posY + height);
				GL11.glTexCoord2f(0,1);
				GL11.glVertex2f(posX, posY + height);
			GL11.glEnd();
		GL11.glPopMatrix();
		Game.endGLMode2D();
	}
	
	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void keyPressed(int key) {
		if (key == Keyboard.KEY_ESCAPE) {
			Game.exitGame();
		}
		
	}

	@Override
	public void keyReleased(int key) {
		// TODO Auto-generated method stub
		
	}
	
	

}
